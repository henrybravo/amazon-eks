#!/bin/bash

# Henry Bravo - August 2019
# Use this script to prepare an EC2 instance with Amazon Linux2 as EKS management 
# client workstation for the ec2-user. No root is required and re-login to use docker
# To install:
#  $ bash <(curl -s https://gitlab.com/henrybravo/amazon-eks/raw/master/prep-ec2-client-eks.sh)
#  or just download the script and execute as ec2-user

set -e

GREEN_TEXT='\e[92m'
RESET_TEXT='\e[0m'

EC2USERHOME='/home/ec2-user'
EC2USERBIN='/home/ec2-user/bin'

echo -e "$GREEN_TEXT"
echo -e "=> getting da tools for ya!"

# getkubectl
echo "get kubectl latest"
# k8s versions
# K8SVERSION=1.10.3
# K8SVERSION=1.11.9
# K8SVERSION=1.12.7
# K8SVERSION = 1.13.7 # released 2019-06-11
# K8SVERSION=1.14.6/2019-08-22
K8SVERSION=1.22.6/2022-03-09
curl -s -o /tmp/kubectl https://s3.us-west-2.amazonaws.com/amazon-eks/$K8SVERSION/bin/linux/amd64/kubectl

# geteksctl
echo "get eksctl latest"
#curl -s --location "https://github.com/weaveworks/eksctl/releases/download/latest_release/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp

# gethelm
#echo "get helm latest"
#curl -s --location "https://get.helm.sh/helm-v2.14.3-linux-amd64.tar.gz" | tar xz -C /tmp

# getiamauthenticator
#echo "get heptio authenticator"
#curl -s -o /tmp/aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.13.7/2019-06-11/bin/linux/amd64/aws-iam-authenticator

# tohomedir
if [ ! -d "$EC2USERBIN" ] ; then
  echo "$EC2USERBIN does not exists, creating"
  mkdir -v $EC2USERBIN
fi
mv -v /tmp/kubectl $EC2USERBIN
mv -v /tmp/eksctl $EC2USERBIN
#mv -v /tmp/aws-iam-authenticator $EC2USERBIN
#mv -v /tmp/linux-amd64/helm $EC2USERBIN
#mv -v /tmp/linux-amd64/tiller $EC2USERBIN
chown -R ec2-user.ec2-user $EC2USERBIN
chmod 0755 $EC2USERBIN/*

# install additional packages
sudo yum install -y git docker jq python-pip gettext
sudo usermod -a -G docker ec2-user
sudo systemctl enable docker
sudo systemctl start docker

# upgrade to latest awscli
pip install awscli --upgrade

# versions
kubectl version
eksctl version
aws version

# finishing touch
echo 'alias k=kubectl' >> $EC2USERHOME/.bashrc
echo "alias l='ls -ltH'" >> $EC2USERHOME/.bashrc
echo "alias la='ls -lAh'" >> $EC2USERHOME/.bashrc
echo "alias ll='ls -lh'" >> $EC2USERHOME/.bashrc
echo "alias ls='ls -G'" >> $EC2USERHOME/.bashrc
echo "alias lsa='ls -lah'" >> $EC2USERHOME/.bashrc
echo "alias md='mkdir -p'" >> $EC2USERHOME/.bashrc
echo "alias please=sudo" >> $EC2USERHOME/.bashrc

echo -e "\n\n=> all should be good now! logout and login to use docker, now go build!\n"
echo -e "$RESET_TEXT"
